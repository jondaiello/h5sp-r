// Main Menu reveal on phones
$(function () {
	$('.menu-link').click(function (e) {
		e.preventDefault();

		$('.global-nav--list').slideToggle('fast');
		return false;
	});
});


// Responsive Slides
$(function () {
  $("#slider").responsiveSlides( {
    auto: true,
    pager: true,
    nav: true,
    speed: 200,
    timeout: 11000,
    namespace: "slides"
  });
});


$(function() {
	// CLEAR SEARCH BOXES
	$('input, textarea').each(function() {
		var default_value = this.value;
		$(this).focus(function() {
			if(this.value == default_value) {
			this.value = '';
			}
		});
		$(this).blur(function() {
			if(this.value == '') {
			this.value = default_value;
			}
		});
	});
});

/*$(document).ready(function () {
	$(".hero-image").imgLiquid({
		fill:true
	});
});*/


$(function() {
	$( "select.styled" ).selectmenu();
});
