<script type="text/javascript">
$(document).ready(function() {

	var modal_log = $('<div />').dialog({
		autoOpen: false,
		modal: true,
		title: 'Log',
		width: 500
	});

	$('.toggle-note').hover(
		function() { $(this).find('a[class!="add-note"]').show(); },
		function() { $(this).find('a[class!="add-note"]').hide(); }
	);

	$('.toggle-note div').toggle(
		function() { $(this).css({ 'line-height':'1.5em', 'white-space':'normal', 'overflow':'show' }); },
		function() { $(this).css({ 'line-height':'1em', 'white-space':'pre', 'overflow':'hidden' }); }
	);

	$('.toggle-note a').click(function() {
		$(this).parent().hide().parent().find('.form-note').show();
		return false;
	});

	$('.form-note .buttons a').click(function() {
		$(this).parent().parent().parent().parent().hide().parent().find('.toggle-note').show();
		return false;
	});

	$('.link-log').click(function() {
		// feedback to user through cursor change
		$('body').css('cursor', 'wait');

		// get page
		$.get(this.href, function( data ) {
			// feedback to user through cursor change
			$('body').css('cursor', 'default');

			// load modal box
			modal_log.html(data);
			modal_log.dialog('open');
		});

		return false;
	});
});
</script>

<style type="text/css">
.toggle-note { position:relative; overflow:hidden; zoom:1.0; }
.toggle-note div { float:left; width:200px; line-height:1em; overflow:hidden; text-overflow:ellipsis; white-space:pre; cursor:pointer; }
.toggle-note a { display:none; position:absolute; top:0px; right:0px; }
.toggle-note a.add-note { display:block; position:relative; }
.form-note { display:none; }
.textfield { padding-bottom:7px; }
.buttons { overflow:hidden; zoom:1.0; }
.buttons a:hover { text-decoration:none; }
</style>

<?php
$this->table->clear();

$this->table->set_template($cp_table_template);
$this->table->set_heading(array(
	lang('safeharbor_lite_heading_backup_start'),
	lang('safeharbor_lite_heading_backup_duration'),
	lang('safeharbor_lite_heading_backup_compression'),
	lang('safeharbor_lite_heading_backup_size'),
	lang('safeharbor_lite_heading_backup_note'),
	lang('safeharbor_lite_heading_backup_actions'),
));

foreach( $backups as $backup )
{
	// format start time
	$backup['start_time'] = date('F j, Y g:ia', $backup['timestamp_start']);

	// format duration
	$backup['duration'] = ($backup['status'] == 'complete') ? timespan($backup['timestamp_start'], $backup['timestamp_end']) : '';

	// format backup size
	$backup['filesize'] = ($backup['status'] == 'complete') ? byte_format(round($backup['filesize']*1024)) : ''; // Kilobytes to Bytes

	// format backup compression
	switch( $backup['extension'] )
	{
		case '.sql':
			$backup['compression'] = 'None';
			break;
		case '.zip':
			$backup['compression'] = 'Zip';
			break;
		case '.tar':
			$backup['compression'] = 'Tar';
			break;
		case '.tgz':
			$backup['compression'] = 'Gzip';
			break;
	}

	// build note view and form
	$note_html  = '';

	if( empty($backup['note']) )
	{
		$note_html .= '<div class="toggle-note"><a class="add-note" href="#">'.lang('safeharbor_lite_label_add_note').'</a></div>';
	}
	else
	{
		$note_html .= '<div class="toggle-note"><div>'.$backup['note'].'</div><a href="#">'.lang('safeharbor_lite_label_edit').'</a></div>';
	}
	$note_html .= '<div class="form-note">';
	$note_html .= form_open('C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=note'.AMP.'ID='.$backup['id']);
	$note_html .= '<div class="textfield">'.form_input('note', $backup['note']).'</div>';
	$note_html .= '<div class="buttons"><span class="button">'.form_submit(array('name'=>'submit', 'value'=>lang('safeharbor_lite_label_save_note'), 'class'=>'submit')).'</span><span class="button"><a class="submit" href="#">'.lang('safeharbor_lite_label_cancel').'</a></span></div>';
	$note_html .= form_close();
	$note_html .= '</div>';
	$backup['note'] = $note_html;

	// build actions for backup
	$backup['actions'] = array();

	if( $backup['status'] == 'complete' )
	{
		$backup['actions'][] = '<a class="link-log" href="'.BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=log'.AMP.'ID='.$backup['id'].'">'.lang('safeharbor_lite_label_log').'</a>';
		$backup['actions'][] = '<a href="'.BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=restore'.AMP.'ID='.$backup['id'].'">'.lang('safeharbor_lite_label_restore').'</a>';
		$backup['actions'][] = '<a href="'.BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=download'.AMP.'ID='.$backup['id'].'">'.lang('safeharbor_lite_label_download').'</a>';
	}

	$backup['actions'] = implode(' | ', $backup['actions']);

	// add table row
	$this->table->add_row(array(
		$backup['start_time'],
		$backup['duration'],
		$backup['compression'],
		$backup['filesize'],
		$backup['note'],
		$backup['actions'],
	));
}

echo $this->table->generate();
?>

<?php if( !$this->input->get('all') ) : ?>
	<p><a href="<?php echo BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=index'.AMP.'all=1'; ?>">View All</a></p>
<?php endif; ?>

<?php include PATH_THIRD.'safeharbor_lite/views/plug.php'; ?>
