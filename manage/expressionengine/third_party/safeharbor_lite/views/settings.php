<?php

echo form_open('C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=settings');

$this->table->clear();
$cp_table_template['heading_cell_start'] = '<th style="width:50%">';

$this->table->set_template($cp_table_template);
$this->table->set_heading('Parameter', 'Value');


/* trigger url */
$array_key = 'trigger_url';
$tbl_param = lang('safeharbor_lite_label_trigger_url');
$tbl_value = form_input(array(
		'name' => $array_key,
		'value' => $settings[$array_key].'&auth='.$settings['auth_code'],
		'readonly' => 'readonly',
		'onclick' => 'javascript:this.focus();this.select();',
	));
$this->table->add_row(array($tbl_param, $tbl_value));
/* /trigger url */

/* notify email */
$array_key = 'notify_email';
$tbl_param = lang('safeharbor_lite_label_notify_email');
$tbl_value = form_input(array(
		'name' => $array_key,
		'value' => $settings[$array_key],
	));
$this->table->add_row(array($tbl_param, $tbl_value));
/* /notify email */

/* attach backup */
$array_key = 'attach_backup';
$tbl_param = lang('safeharbor_lite_label_attach_backup');
$tbl_value = form_dropdown($array_key, array('No','Yes'), $settings[$array_key]);
$this->table->add_row(array($tbl_param, $tbl_value));
/* /attach backup */

/* compression */
$array_key = 'compression';
$tbl_param = lang('safeharbor_lite_label_compression');
$tbl_value = form_dropdown($array_key, array('none'=>'None', 'zip'=>'Zip', 'gzip'=>'Gzip'), $settings[$array_key]);
$this->table->add_row(array($tbl_param, $tbl_value));
/* /compression */


// clean up
$tbl_param = NULL;
$tbl_value = NULL;
unset($tbl_param, $tbl_value);

echo $this->table->generate();
echo form_submit(array('name'=>'submit', 'value'=>lang('safeharbor_lite_label_submit'), 'class'=>'submit'));
echo form_close();

include PATH_THIRD.'safeharbor_lite/views/plug.php';
