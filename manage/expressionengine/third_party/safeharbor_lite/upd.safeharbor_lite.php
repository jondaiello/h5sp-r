<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

class Safeharbor_lite_upd
{
	public $version = '1.0.2';

	public function __construct()
	{
		$this->EE =& get_instance();
		$this->backups_directory = APPPATH.'cache/safeharbor_lite';
	}

	public function install()
	{
		// create backup directory and secure with htaccess
		@mkdir($this->backups_directory);

		$htaccess = $this->backups_directory.'/.htaccess';
		@file_put_contents($htaccess, 'deny from all');

		// install module
		$this->EE->db->insert('modules', array(
			'module_name' => 'Safeharbor_lite',
			'module_version' => $this->version,
			'has_cp_backend' => 'y',
			'has_publish_fields' => 'n'
		));

		// create cron action
		$this->EE->db->insert('actions', array(
			'class' => 'Safeharbor_lite',
			'method' => 'communicate',
		));

		// create backups table
		$this->EE->db->query("CREATE TABLE IF NOT EXISTS `".$this->EE->db->dbprefix('safeharbor_lite_backups')."` (
			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`site_id` int(11) NOT NULL DEFAULT '0',
			`status` enum('running','complete','error') NOT NULL DEFAULT 'running',
			`name` varchar(25) NOT NULL DEFAULT '',
			`md5` varchar(32) NOT NULL DEFAULT '',
			`filesize` int(10) NOT NULL DEFAULT '0',
			`timestamp_start` int(10) NOT NULL DEFAULT '0',
			`timestamp_end` int(10) NOT NULL DEFAULT '0',
			`log` text NOT NULL,
			`note` text NOT NULL,
			`extension` enum('.sql','.zip','.tgz') NOT NULL DEFAULT '.sql',
			PRIMARY KEY (`id`),
			KEY `site_id` (`site_id`),
			KEY `status` (`status`));");

		// create settings table
		$this->EE->db->query("CREATE TABLE IF NOT EXISTS `".$this->EE->db->dbprefix('safeharbor_lite_settings')."` (
			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`site_id` int(11) NOT NULL DEFAULT '0',
			`notify_email` varchar(150) NOT NULL DEFAULT '',
			`attach_backup` enum('0','1') NOT NULL DEFAULT '0',
			`compression` enum('none','zip','gzip') NOT NULL DEFAULT 'none',
			`auth_code` varchar(20) NOT NULL DEFAULT '',
			PRIMARY KEY (`id`),
			KEY `site_id` (`site_id`));");

		return TRUE;
	}

	public function update( $current = '' )
	{
		if($current == $this->version) { return FALSE; }
		return TRUE;
	}

	public function uninstall()
	{
		// remove backups directory
		$this->EE->functions->delete_directory($this->backups_directory, TRUE);

		// uninstall module
		$module_id = $this->EE->db->query("SELECT module_id FROM ".$this->EE->db->dbprefix('modules')." WHERE module_name='Safeharbor_lite' LIMIT 1");
		$module_id = $module_id->row('module_id');

		$this->EE->db->query("DELETE FROM ".$this->EE->db->dbprefix('module_member_groups')." WHERE module_id = $module_id");
		$this->EE->db->query("DELETE FROM ".$this->EE->db->dbprefix('modules')." WHERE module_name = 'Safeharbor_lite'");
		$this->EE->db->query("DELETE FROM ".$this->EE->db->dbprefix('actions')." WHERE class = 'Safeharbor_lite'");

		// remove module specific tables
		$this->EE->db->query("DROP TABLE IF EXISTS ".$this->EE->db->dbprefix('safeharbor_lite_backups'));
		$this->EE->db->query("DROP TABLE IF EXISTS ".$this->EE->db->dbprefix('safeharbor_lite_settings'));

		return TRUE;
	}
}

/* End of File: upd.safeharbor_lite.php */
