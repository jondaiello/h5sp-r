<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'mod.safeharbor_lite.php';

class Safeharbor_lite_mcp
{
	private $safeharbor_mod;

	// data to be sent to the views
	private $data = array();

	public function __construct()
	{
		// load the expressionengine object
		$this->EE =& get_instance();

		// load the safeharbor object
		$this->safeharbor_mod = new Safeharbor_lite();

		// load settings
		$this->safeharbor_mod->_get_settings();
	}

	public function index()
	{
		// check if safeharbor has been configured
		if( empty($this->safeharbor_mod->settings['notify_email']) )
		{
			$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=settings');
		}

		// load libraries
		$this->EE->load->library('table');
		$this->EE->load->helper(array('number','date'));

		// Set page title
		if(version_compare(APP_VER, '2.6.0', '<'))
		{
			$this->EE->cp->set_variable('cp_page_title', $this->EE->lang->line('safeharbor_lite_page_title_index'));
		}
		else
		{
			$this->EE->view->cp_page_title = $this->EE->lang->line('safeharbor_lite_page_title_index');
		}

		// set navigation
		$this->EE->cp->set_right_nav(array(
			'safeharbor_lite_nav_home' => BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=index',
			'safeharbor_lite_nav_settings' => BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=settings',
			'safeharbor_lite_nav_backup' => BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=backup',
		));

		$this->EE->cp->add_js_script(array('ui' => array('core', 'position', 'widget')));

		// send data to view
		$this->data['backups'] = $this->EE->input->get('all') ? $this->safeharbor_mod->_get_backups() : $this->safeharbor_mod->_get_backups(10);
		$this->data['settings'] = $this->safeharbor_mod->_get_settings();
		$this->data['class'] = $this;

		return $this->EE->load->view('index', $this->data, TRUE);
	}

	public function settings()
	{
		// load libraries
		$this->EE->load->library('form_validation');
		$this->EE->load->library('table');

		// setup validation
		$this->EE->form_validation->set_rules('notify_email', 'notify email', 'required|valid_email');

		// process form submission
		if( $this->EE->input->post('submit') )
		{
			if( $this->EE->form_validation->run() )
			{
				$this->safeharbor_mod->settings['notify_email'] = $this->EE->input->post('notify_email');
				$this->safeharbor_mod->settings['attach_backup'] = $this->EE->input->post('attach_backup');
				$this->safeharbor_mod->settings['compression'] = $this->EE->input->post('compression');

				if( $this->safeharbor_mod->_save_settings() )
				{
					$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('safeharbor_lite_config_save_success'));
				}
				else
				{
					$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('safeharbor_lite_config_save_failure'));
				}
			}
			else
			{
				$this->EE->session->set_flashdata('message_failure', validation_errors());
			}

			$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=settings');
		}



		// Set page title
		if(version_compare(APP_VER, '2.6.0', '<'))
		{
			$this->EE->cp->set_variable('cp_page_title', $this->EE->lang->line('safeharbor_lite_page_title_settings'));
		}
		else
		{
			$this->EE->view->cp_page_title = $this->EE->lang->line('safeharbor_lite_page_title_settings');
		}


		// set navigation
		$this->EE->cp->set_right_nav(array(
			'safeharbor_lite_nav_home' => BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=index',
			'safeharbor_lite_nav_settings' => BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=settings',
			'safeharbor_lite_nav_backup' => BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=backup',
		));

		// send data to view
		$this->data['settings'] = $this->safeharbor_mod->settings;

		return $this->EE->load->view('settings', $this->data, TRUE);
	}

	public function backup()
	{
		// check if safeharbor has been configured
		if( empty($this->safeharbor_mod->settings['notify_email']) )
		{
			$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=settings');
		}

		if( $this->safeharbor_mod->_backup_database() )
		{
			$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('safeharbor_lite_backup_success'));
		}
		else
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('safeharbor_lite_backup_failure'));
		}

		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=index');
	}

	public function restore()
	{
		// check if safeharbor has been configured
		if( empty($this->safeharbor_mod->settings['notify_email']) )
		{
			$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=settings');
		}

		// load libraries
		$this->EE->load->library('form_validation');
		$this->EE->load->helper('date');

		// get backup information
		$backup_id = (int)$this->EE->input->get('ID');

		if( !empty($backup_id) )
		{
			// check if backup is valid
			$this->data['backup'] = $this->EE->db->get_where('safeharbor_lite_backups', array('id'=>$backup_id), 1);
			$this->data['backup'] = $this->data['backup']->row_array();

			$this->EE->form_validation->set_rules('confirm', 'confirm', 'required');

			if( $this->EE->input->post('submit') )
			{
				if( $this->EE->form_validation->run() )
				{
					if( $this->safeharbor_mod->_restore_database($backup_id) )
					{
						$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=index');
					}
					else
					{
						$this->EE->session->set_flashdata('message_failure', $this->safeharbor_mod->error);
						$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=restore'.AMP.'ID='.$backup_id);
					}
				}
				else
				{
					$this->EE->session->set_flashdata('message_failure', validation_errors());
					$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=restore'.AMP.'ID='.$backup_id);
				}
			}
		}

		// Set page title
		if(version_compare(APP_VER, '2.6.0', '<'))
		{
			$this->EE->cp->set_variable('cp_page_title', $this->EE->lang->line('safeharbor_lite_page_title_restore'));
		}
		else
		{
			$this->EE->view->cp_page_title = $this->EE->lang->line('safeharbor_lite_page_title_restore');
		}

		// set navigation
		$this->EE->cp->set_right_nav(array(
			'safeharbor_lite_nav_home' => BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=index',
			'safeharbor_lite_nav_settings' => BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=settings',
			'safeharbor_lite_nav_backup' => BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=backup',
		));

		return $this->EE->load->view('restore', $this->data, TRUE);
	}

	public function note()
	{
		$backup_id = (int)$this->EE->input->get('ID');
		$note = $this->EE->input->post('note');

		$this->EE->db->where('id', $backup_id);
		$this->EE->db->update('safeharbor_lite_backups', array('note' => $note));

		$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('safeharbor_lite_note_save_success'));
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=safeharbor_lite'.AMP.'method=index');
	}

	public function log()
	{
		$backup_id = (int)$this->EE->input->get('ID');

		$backup = $this->EE->db->get_where('safeharbor_lite_backups', array('id'=>$backup_id), 1);
		$backup = $backup->row_array();

		if( ob_get_length() > 0 ) ob_end_clean();
		print '<pre style="padding-bottom:1em; overflow:auto;">';
		print $backup['log'];
		print '</pre>';
		exit;
	}

	public function download()
	{
		$backup_id = (int)$this->EE->input->get('ID');
		$backup_path = APPPATH.'cache/safeharbor_lite/';

		$backup = $this->EE->db->get_where('safeharbor_lite_backups', array('id' => $backup_id), 1);
		$backup = $backup->row_array();

		$backup_name = $backup['name'] . $backup['extension'];
		$backup_file = $backup_path . $backup_name;

		// determine mime type
		$this->EE->load->helper('file');
		$backup_mime = get_mime_by_extension($backup_name);
		$backup_mime = empty($backup_mime) ? 'application-octet-stream' : $backup_mime;

		// send file download to user
		header('Content-Type: "'.$backup_mime.'"');
		header('Cache-Control: public');
		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename='.$backup_name);
		header('Content-Transfer-Encoding: binary');

		$this->safeharbor_mod->_readfile_chunked($backup_file);
	}
}

/* End of File: mcp.safeharbor_lite.php */
