<?php
$lang = array(

// Required for MODULES page
'safeharbor_lite_module_name' => 'Safe Harbor Lite',
'safeharbor_lite_module_description' => 'Database backups for your ExpressionEngine website.',

//----------------------------------------

// Additional Key => Value pairs go here

'safeharbor_lite_nav_home' => 'Home',
'safeharbor_lite_nav_settings' => 'Settings',
'safeharbor_lite_nav_backup' => 'Backup',

'safeharbor_lite_page_title_index' => 'Safe Harbor Lite',
'safeharbor_lite_page_title_settings' => 'Safe Harbor Lite Settings',
'safeharbor_lite_page_title_backup' => 'Safe Harbor Lite Backup',
'safeharbor_lite_page_title_restore' => 'Safe Harbor Lite Restore',

'safeharbor_lite_label_trigger_url' => 'Trigger URL',
'safeharbor_lite_label_notify_email' => 'Notification Email Address',
'safeharbor_lite_label_attach_backup' => 'Attach backup to email?',
'safeharbor_lite_label_compression' => 'Compress backup?',

'safeharbor_lite_label_submit' => 'Submit',
'safeharbor_lite_label_restore' => 'Restore',
'safeharbor_lite_label_cancel' => 'Cancel',
'safeharbor_lite_label_save_note' => 'Save Note',
'safeharbor_lite_label_add_note' => 'Add Note',
'safeharbor_lite_label_download' => 'Download',
'safeharbor_lite_label_edit' => 'Edit',
'safeharbor_lite_label_cancel' => 'Cancel',
'safeharbor_lite_label_log' => 'Log',
'safeharbor_lite_label_restore' => 'Restore',

'safeharbor_lite_heading_backup_start' => 'Backup Date',
'safeharbor_lite_heading_backup_duration' => 'Duration',
'safeharbor_lite_heading_backup_size' => 'Size',
'safeharbor_lite_heading_backup_compression' => 'Compression',
'safeharbor_lite_heading_backup_note' => 'Note',
'safeharbor_lite_heading_backup_actions' => 'Actions',

'safeharbor_lite_config_save_success' => 'Configuration Saved!',
'safeharbor_lite_config_save_failure' => 'Unable to save configuration',

'safeharbor_lite_note_save_success' => 'Note Saved!',
'safeharbor_lite_note_save_failure' => 'Unable to save note',

// END
'' => ''
);

/* End of File: lang.safeharbor_lite.php */
