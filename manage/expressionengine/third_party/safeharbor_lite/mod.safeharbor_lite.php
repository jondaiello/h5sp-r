<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

class Safeharbor_lite
{
	public $return_data = NULL;

	public $settings = array();
	public $dump_file = '';

	public $log = array();
	public $error = '';

	public function __construct()
	{
		// load the expressionengine object
		$this->EE =& get_instance();

		// load settings
		$this->_get_settings();
	}

	public function communicate()
	{
		// validate auth code
		if( $this->EE->input->get('auth') != $this->settings['auth_code'] )
		{
			$this->EE->output->set_header('HTTP/1.1: 401 Bad Request');
			$this->EE->output->set_status_header(401);
			return FALSE;
		}

		// run backup
		if( !$this->_backup_database() )
		{
			$this->EE->output->set_header('HTTP/1.1: 500 Internal Server Error');
			$this->EE->output->set_status_header(500);
			return FALSE;
		}

		return TRUE;
	}

	public function _restore_database( $backup_id=NULL )
	{
		$return = TRUE;

		// get backup information
		$backup = $this->EE->db->get_where('safeharbor_lite_backups', array('id'=>$backup_id), 1);
		$backup = $backup->row_array();

		// make sure we can run a restore successfully
		// 1. check if exec exists. we cannot continue the restore process unless it does
		if( !function_exists('exec') )
		{
			$this->error = 'Failed to restore: The PHP function exec() is unavailable.';
			return FALSE;
		}

		// 2. check if mysql command is available. we cannot continue the restore process without it
		exec('command -v mysql', $output);
		if( empty($output) )
		{
			$this->error = 'Failed to restore: The mysql command is not available on your server.';
			return FALSE;
		}

		// 3. if our backup is a .zip, make sure the unzip command is available.
		if( $backup['extension'] == '.zip' )
		{
			// check if unzip exists
			exec('command -v unzip', $output);
			if( empty($output) )
			{
				$this->error = 'Failed to restore: The unzip command is not available on your server.';
				return FALSE;
			}
		}

		// backup current safeharbor tables to restore after full database restore
		$safeharbor_sql = $this->_backup_database(TRUE);

		// identify the database file to be restored. decompress selected backup
		// if needed.
		if( $backup['extension'] == '.tgz' )
		{
			// decompress file
			chdir(APPPATH.'cache/safeharbor_lite');
			exec("tar -pxzf ".$backup['name'].$backup['extension']." ".$backup['name'].'.sql');
		}

		if( $backup['extension'] == '.zip' )
		{
			// decompress file
			chdir(APPPATH.'cache/safeharbor_lite');
			exec("unzip ".$backup['name'].$backup['extension']);
		}

		$backup_sql = APPPATH.'cache/safeharbor_lite/'.$backup['name'].'.sql';

		// drop all tables
		if( $this->_empty_database() === FALSE )
		{
			$this->error = 'Failed to restore: Unable to clear the current database.';
			return FALSE;
		}

		$db_name = $this->EE->db->database;
		$db_host = $this->EE->db->hostname;
		$db_user = $this->EE->db->username;
		$db_pass = $this->EE->db->password;

		// restore full database dump
		exec("mysql --host=$db_host --user=$db_user --password=$db_pass --database=$db_name < $backup_sql", $output, $return_status);
		if( $return_status !== 0 )
		{
			$this->error = 'Failed to restore backup.';
			return FALSE;
		}

		// restore safeharbor database dump
		exec("mysql --host=$db_host --user=$db_user --password=$db_pass --database=$db_name < $safeharbor_sql", $output, $return_status);
		if( $return_status !== 0 )
		{
			$this->error = 'Failed to restore Safe Harbor\'s previous state.';
			return FALSE;
		}

		// remove sql files
		unlink($safeharbor_sql);
		unlink($backup_sql);

		return $return;
	}

	public function _empty_database()
	{
		$return = TRUE;

		// get table names
		$results = $this->EE->db->query('SHOW TABLES');
		$results = $results->result();

		$tables = array();
		if( !empty($results) )
		{
			foreach( $results as $row )
			{
				foreach( $row as $table )
				{
					// drop table. database class will return true if table was
					// dropped
					$return = $this->EE->db->query("DROP TABLE IF EXISTS $table");
				}
			}
		}

		return $return;
	}

	public function _backup_database( $module_only=FALSE )
	{
		$return = NULL;
		$backup_info = array();

		$backup_info['timestamp_start'] = time();
		$this->_log('backup started');

		$backup_info['name'] = 'safeharbor_'.$backup_info['timestamp_start'];

		$this->dump_file = APPPATH.'cache/safeharbor_lite/'.$backup_info['name'].'.sql';
		$this->_log('dumping database to '.$this->dump_file);

		// we don't need a database entry for backing up safeharbor's tables.
		// this backup is only temporary
		if( $module_only === FALSE )
		{
			// set backup entry as 'running'
			$backup_info['status'] = 'running';
			$this->EE->db->insert('safeharbor_lite_backups', $backup_info);
			$backup_info['id'] = $this->EE->db->insert_id();
		}

		// if exec exists and is enabled, try doing a mysql dump otherwise
		// fallback to php based database dump
		if( function_exists('exec') )
		{
			$this->_log('exec() exists and is enabled.');

			// check if mysqldump command is available
			exec('command -v mysqldump', $output);
			$this->_log('Checking for mysqldump command.');

			// if mysqldump is available, use it to dump the database otherwise
			// fallback to php based database dump
			if( !empty($output) )
			{
				$this->_log('mysqldump command exists.');
				$return = $this->_mysql_dump($module_only);
			}
			else
			{
				$this->_log('mysqldump command does not exist.');
				$return = $this->_php_dump($module_only);
			}
		}
		else
		{
			$this->_log('exec() does not exist.');
			$return = $this->_php_dump($module_only);
		}

		// return the file name if we are only backing up safeharbor's tables
		if( $module_only === TRUE )
		{
			return $this->dump_file;
		}

		if( $return === TRUE AND $module_only === FALSE )
		{
			$this->_log('backup completed successfully.');

			// compress the backup (if enabled)
			if( $this->settings['compression'] != 'none' )
			{
				$this->_log('compressing the backup is enabled.');

				$current_dump_file = $this->dump_file;

				// compress dump file using zip
				if( $this->settings['compression'] == 'zip' )
				{
					$this->_log('compressing backup using zip.');

					$backup_info['extension'] = '.zip';
					$dump_file = str_replace('.sql', $backup_info['extension'], $current_dump_file);

					// compress file
					$this->EE->load->library('zip');
					$this->EE->zip->read_file($current_dump_file);
					$this->EE->zip->archive($dump_file);
					$this->EE->zip->clear_data();
				}

				// compress dump file using gzip
				if( $this->settings['compression'] == 'gzip' )
				{
					$this->_log('compressing backup using gzip.');

					$backup_info['extension'] = '.tgz';
					$dump_file = str_replace('.sql', $backup_info['extension'], $current_dump_file);

					// compress file
					chdir(APPPATH.'cache/safeharbor_lite');
					exec("tar -pczf ".basename($dump_file)." ".basename($current_dump_file));
				}

				// remove original dump file
				if( file_exists($current_dump_file) )
				{
					$this->_log('removing original dump file');
					unlink($current_dump_file);
				}
			}
			else
			{
				$backup_info['extension'] = '.sql';
				$dump_file = $this->dump_file;
			}

			// backup information
			$backup_info['timestamp_end'] = time();
			$backup_info['site_id'] = $this->EE->config->config['site_id'];

			$backup_info['status'] = 'complete';
			$backup_info['md5'] = md5_file($dump_file);
			$backup_info['filesize'] = round(filesize($dump_file)/1024); // size in KB
			$backup_info['log'] = $this->_log();

			$this->EE->db->where('id', $backup_info['id']);
			$this->EE->db->update('safeharbor_lite_backups', $backup_info);
		}
		else
		{
			$this->_log('backup failed.');

			$backup_info['status'] = 'error';
			$backup_info['md5'] = '';
			$backup_info['filesize'] = 0;
			$backup_info['log'] = $this->_log();

			$this->EE->db->where('id', $backup_info['id']);
			$this->EE->db->update('safeharbor_lite_backups', $backup_info);
		}

		// send backup complete notification
		$this->_send_notification($backup_info);

		return $return;
	}

	public function _mysql_dump( $module_only=FALSE )
	{
		$this->_log('Performing backup using mysqldump.');

		$db_name = $this->EE->db->database;
		$db_host = $this->EE->db->hostname;
		$db_user = $this->EE->db->username;
		$db_pass = $this->EE->db->password;

		// if we are dumping just safeharbor's tables, we need to modify the
		// dump command
		if( $module_only === TRUE )
		{
			$safeharbor_tables = ' '.$this->EE->db->dbprefix('safeharbor_lite_backups').' '.$this->EE->db->dbprefix('safeharbor_lite_settings').' ';
		}

		$command = "mysqldump --add-drop-table --host=$db_host --user='$db_user' --password='$db_pass' $db_name".(!empty($safeharbor_tables) ? $safeharbor_tables : ' ')."> " . $this->dump_file;
		exec($command);

		// make sure the dump completed, if not fallback to the php dump
		if( !$this->_verify_backup() )
		{
			$this->_log('mysqldump command failed.');
			return $this->_php_dump();
		}

		return TRUE;
	}

	public function _php_dump( $module_only=FALSE )
	{
		$this->_log('Performing backup using PHP.');

		// get table names
		$results = $this->EE->db->query('SHOW TABLES');
		$results = $results->result();

		$tables = array();
		if( !empty($results) )
		{
			foreach( $results as $row )
			{
				foreach( $row as $table )
				{
					// if we are backing up only safeharbor's tables, skip the
					// rest.
					if( $module_only === TRUE AND ($table != $this->EE->db->dbprefix('safeharbor_lite_backups') OR $table != $this->EE->db->dbprefix('safeharbor_lite_settings')) )
					{
						continue;
					}

					$tables[] = $table;
				}
			}
		}

		// clean up
		$results = NULL;
		unset($results);

		// make sure we have tables to backup
		if( empty($tables) )
		{
			$this->_log('There are no tables to backup');
			return FALSE;
		}

		// check if we can create the dump file
		if( touch($this->dump_file) === FALSE )
		{
			$this->_log('Unable to create dump file: '.$this->dump_file);
			return FALSE;
		}

		// open the dump file for writing
		$file = fopen($this->dump_file, 'w');

		foreach( $tables as $table )
		{
			// build create table syntax
			$create_table_results = $this->EE->db->query('SHOW CREATE TABLE '.$table);

			$create_table_string  = "\n\n";
			$create_table_string .= $create_table_results->row('Create Table');

			$create_table_string  = str_replace('CREATE TABLE `'.$table.'`', 'CREATE TABLE IF NOT EXISTS `'.$table.'`', $create_table_string);
			$create_table_string .= ";\n";

			fwrite($file, $create_table_string);

			// clean up
			$create_table_string = NULL;
			unset($create_table_string);

			// build insert rows syntax
			$num_rows = $this->EE->db->query('SELECT COUNT(*) AS rows FROM '.$table);
			$num_rows = $num_rows->row('rows');

			$loop = ceil($num_rows/50);

			for( $x=0; $x<=$loop; $x++ )
			{
					$results = $this->EE->db->query('SELECT * FROM '.$table.' LIMIT '.($x*50).', 50');
					$results = $results->result();

					foreach( $results as $row )
					{
							fwrite($file, "\n".$this->EE->db->insert_string($table, $row).';');
					}

					// clean up
					$results = NULL;
					unset($results);
			}

			// clean up
			$num_rows = NULL;
			$loop = NULL;
			unset($num_rows, $loop);
		}

		// close the dump file
		fclose($file);

		// make sure the dump completed
		if( !$this->_verify_backup() )
		{
			$this->_log('PHP database dump failed.');
			return FALSE;
		}

		return TRUE;
	}

	public function _verify_backup()
	{
		if( !file_exists($this->dump_file) AND filesize($this->dump_file) < 10240 )
		{
			$this->_log('Failed to verify backup.');
			return FALSE;
		}

		$this->_log('Backup verified.');
		return TRUE;
	}

	public function _get_settings()
	{
		if( empty($this->settings) )
		{
			$site_id = $this->EE->config->config['site_id'];

			// retrieve settings from database
			$settings = $this->EE->db->get_where('safeharbor_lite_settings', array('site_id'=>$site_id), 1);
			$settings = $settings->row_array();

			// build trigger url
			$trigger_url = $this->EE->functions->fetch_site_index(0);

			$action_id = $this->EE->db->get_where('actions', array('class'=>'Safeharbor_lite', 'method'=>'communicate'), 1);
			$action_id = $action_id->row('action_id');

			if( strpos($trigger_url, 'index.php') === FALSE )
			{
				$trigger_url .= 'index.php';
			}

			$trigger_url .= QUERY_MARKER.'ACT='.$action_id;

			// generate an auth_code for the cron url
			$auth_code = strtoupper(substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 8));

			// default settings
			$defaults = array(
				'site_id' => $site_id,
				'trigger_url' => $trigger_url,
				'auth_code' => $auth_code,

				'notify_email' => '',
				'attach_backup' => '',
				'compression' => '',
			);

			$this->settings = array_merge($defaults, $settings);

			// cleanup
			$defaults = NULL;
			$settings = NULL;
			unset($defaults, $settings);
		}

		return $this->settings;
	}

	public function _save_settings()
	{
		// remove unnecessary settings
		unset($this->settings['trigger_url']);

		//save settings to local database
		$query_string = $this->EE->db->insert_string('safeharbor_lite_settings', $this->settings);
		$query_string = preg_replace('/^INSERT/', 'REPLACE', $query_string);
		$this->EE->db->query($query_string);

		return TRUE;
	}

	public function _send_notification( $backup )
	{
		$this->_log('Sending email notification');

		// load libraries
		$this->EE->load->library('email');
		$this->EE->load->helper(array('text', 'number', 'date'));

		// email data
		$email_data['site'] = str_replace('index.php', '', $this->EE->functions->fetch_site_index());
		$email_data['date'] = date('F, j, Y');
		$email_data['backup_status'] = $backup['status'];
		$email_data['backup_file'] = $backup['name'] . $backup['extension'];
		$email_data['backup_size'] = byte_format(round($backup['filesize']*1024));
		$email_data['backup_start'] = date('F j, Y g:ia', $backup['timestamp_start']);
		$email_data['backup_end'] = date('F j, Y g:ia', $backup['timestamp_end']);
		$email_data['backup_duration'] = timespan($backup['timestamp_start'], $backup['timestamp_end']);

		$text_message = $this->EE->load->view('email_message_text', $email_data, TRUE);
		$html_message = $this->EE->load->view('email_message_html', $email_data, TRUE);

		$this->EE->email->initialize(array('mailtype' => 'html'));

		$this->EE->email->from('info@eeharbor.com');
		$this->EE->email->to($this->settings['notify_email']);

		$this->EE->email->subject('Safe Harbor Backup Report');
		$this->EE->email->message($html_message);
		$this->EE->email->set_alt_message(word_wrap(entities_to_ascii($text_message), 70));

		// check if the user wants to attach the backup with the email. If the
		// backup is greater than 10MB, we can't send it, regardless of the
		// setting.
		if( !empty($this->settings['attach_backup']) AND $backup['filesize'] < 10240 )
		{
			$this->_log('Attaching backup file to email');
			$this->EE->email->attach(APPPATH.'cache/safeharbor_lite/'.$backup['name'].$backup['extension']);
		}

		$this->EE->email->send();
	}

	public function _get_backups( $limit=NULL, $where=NULL )
	{
		$this->EE->db->order_by('id', 'desc');
		$this->EE->db->where(array('site_id'=>$this->settings['site_id'], 'status !='=>'running'));

		if( !empty($where) )
		{
			$this->EE->db->where($where);
		}

		$this->EE->db->limit($limit);

		$backups = $this->EE->db->get('safeharbor_lite_backups');
		return $backups->result_array();
	}

	public function _readfile_chunked( $filename )
	{
		$rate = 1024;

		if( file_exists($filename) AND is_file($filename) )
		{
			$file = fopen($filename, 'rb');

			while( !feof($file) )
			{
				print fread($file, round($rate * 1024));
				flush();
				ob_flush();
				sleep(1);
			}

			fclose($file);
		}

		exit;
	}

	public function _log( $msg=NULL )
	{
		if( !empty($msg) )
		{
			$this->log[] = date('Y-m-d H:i:s').' --> '.strtolower($msg);
		}
		else
		{
			return implode("\n", $this->log);
		}
	}
}

/* End of File: mod.safeharbor_lite.php */
