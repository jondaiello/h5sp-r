// stylesSet URL => my_styles:/themes/third_party/wygwam_assets/my_styles.js

CKEDITOR.stylesSet.add( 'my_styles',
[
    // Inline Styles
    { name: 'Float Right', element: 'img', attributes: { 'class': 'right' } },
]);